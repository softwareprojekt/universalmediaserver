#!/bin/sh

auswahl=3
echo "Möchten sie das OpenJDK-7 oder Oracle JDK 8 installieren?"
echo "1    openJDK 7"
echo "2    Oracle JDK 8"
echo "3    nichts"
read auswahl

if [ "$auswahl" = 1 ]; then
	sudo apt-get update
	sudo apt-get install openjdk-7-jre
elif [ "$auswahl" = 2 ]; then
	sudo add-apt-repository ppa:webupd8team/java
	sudo apt-get update
	sudo apt-get install oracle-java8-installer
fi 


sudo apt-get install mediainfo mplayer
sudo apt-get install dcraw vlc

