package net.pms.remote;

import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.util.List;
import net.pms.PMS;
import net.pms.configuration.FormatConfiguration;
import net.pms.configuration.PmsConfiguration;
import net.pms.dlna.DLNAMediaInfo;
import net.pms.dlna.DLNAResource;
import net.pms.dlna.RootFolder;
import net.pms.dlna.virtual.VirtualVideoAction;
import net.pms.encoders.FFMpegVideo;
import net.pms.encoders.Player;
import net.pms.formats.v2.SubtitleUtils;
import net.pms.io.OutputParams;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RemotePlayHandler implements HttpHandler {
	private static final Logger LOGGER = LoggerFactory.getLogger(RemotePlayHandler.class);
	private final static String CRLF = "\r\n";
	private RemoteWeb parent;
	private static final PmsConfiguration configuration = PMS.getConfiguration();

	public RemotePlayHandler(RemoteWeb parent) {
		this.parent = parent;
	}

	private void endPage(StringBuilder sb) {
		sb.append("</div>").append(CRLF);
		sb.append("</div>").append(CRLF);
		sb.append("</body>").append(CRLF);
		sb.append("</html>").append(CRLF);
	}

	private String mkPage(String id, HttpExchange t) throws IOException {
		boolean flowplayer = true;

		LOGGER.debug("make play page " + id);
		RootFolder root = parent.getRoot(RemoteUtil.userName(t));
		if (root == null) {
			throw new IOException("Unknown root");
		}
		String id1 = id;
		List<DLNAResource> res = root.getDLNAResources(id, false, 0, 0, root.getDefaultRenderer());
		String rawId = id;

		DLNAResource r = res.get(0);
		// hack here to ensure we got a root folder to use for recently played etc.
		root.getDefaultRenderer().setRootFolder(root);
		String mime = root.getDefaultRenderer().getMimeType(r.mimeType());
		String mediaType = "";
		String coverImage = "";
		if (r instanceof VirtualVideoAction) {
			// for VVA we just call the enable fun directly
			// waste of resource to play dummy video
			((VirtualVideoAction) r).enable();
			// special page to return
			return "<html><head><script>window.refresh=true;history.back()</script></head></html>";
		}
		if(r.getFormat().isImage()) {
			flowplayer = false;
			coverImage = "<img src=\"/raw/" + rawId + "\" alt=\"\"><br>";
		}
		if (r.getFormat().isAudio()) {
			mediaType = "audio";
			String thumb = "/thumb/" + id1;
			coverImage = "<img src=\"" + thumb + "\" alt=\"\"><br>";
			flowplayer = false;
		}
		if (r.getFormat().isVideo()) {
			mediaType = "video";
			if(mime.equals(FormatConfiguration.MIMETYPE_AUTO)) {
				if (r.getMedia() != null && r.getMedia().getMimeType() != null) {
					mime = r.getMedia().getMimeType();
				}
			}
			/*if(!RemoteUtil.directmime(mime)) {
				mime = RemoteUtil.MIME_TRANS;
				flowplayer = false;
			} */
		}

		// Media player HTML
		StringBuilder sb = new StringBuilder();
		sb.append("<!DOCTYPE html>").append(CRLF);
			sb.append("<head>").append(CRLF);
				sb.append("<link rel=\"stylesheet\" href=\"/files/reset.css\" type=\"text/css\" media=\"screen\">").append(CRLF);
				sb.append("<link rel=\"stylesheet\" href=\"/files/web.css\" type=\"text/css\" media=\"screen\">").append(CRLF);
				sb.append("<link rel=\"stylesheet\" href=\"/files/web-narrow.css\" type=\"text/css\" media=\"screen and (max-width: 1080px)\">").append(CRLF);
				sb.append("<link rel=\"stylesheet\" href=\"/files/web-wide.css\" type=\"text/css\" media=\"screen and (min-width: 1081px)\">").append(CRLF);
				sb.append("<link rel=\"icon\" href=\"/files/favicon.ico\" type=\"image/x-icon\">").append(CRLF);
				sb.append("<title>Universal Media Server</title>").append(CRLF);
				if (flowplayer) {
					sb.append("<script src=\"/files/jquery.min.js\"></script>").append(CRLF);
					sb.append("<script src=\"/files/flowplayer.min.js\"></script>").append(CRLF);
					sb.append("<link rel=\"stylesheet\" href=\"/files/functional.css\">").append(CRLF);
				}
			sb.append("</head>").append(CRLF);
			sb.append("<body id=\"ContentPage\">").append(CRLF);
				sb.append("<div id=\"Container\">").append(CRLF);
					sb.append("<div id=\"Menu\">").append(CRLF);
						sb.append("<a href=\"/browse/0\" id=\"HomeButton\"></a>").append(CRLF);
					sb.append("</div>").append(CRLF);
					sb.append("<div id=\"VideoContainer\">").append(CRLF);
					// for video this gives just an empty line
					sb.append(coverImage).append(CRLF);
					if(r.getFormat().isImage()) {
						// do this like this to simplify the code
						// skip all player crap since img tag works well
						endPage(sb);
						return sb.toString();
					}

					if (flowplayer) {
						//sb.append("<div class=\"flowplayer no-time no-volume no-mute\" data-ratio=\"0.5625\" data-embed=\"false\" data-flashfit=\"true\">").append(CRLF);
						sb.append("<div class=\"player\">").append(CRLF);
					}
					sb.append("<").append(mediaType);
					if (flowplayer) {
						sb.append(" controls autoplay>").append(CRLF);
						if (
							RemoteUtil.directmime(mime) &&
							!transMp4(mime, r.getMedia()) &&
							!r.isResume()
						) {
							sb.append("<source src=\"/media/").append(URLEncoder.encode(id1, "UTF-8")).append("\" type=\"").append(mime).append("\">").append(CRLF);
						}
						sb.append("<source src=\"/fmedia/").append(URLEncoder.encode(id1, "UTF-8")).append("\" type=\"video/flash\">");
					} else {
						sb.append(" width=\"720\" height=\"404\" controls autoplay>").append(CRLF);
						sb.append("<source src=\"/media/").append(URLEncoder.encode(id1, "UTF-8")).append("\" type=\"").append(mime).append("\">");
					}
					sb.append(CRLF);

					if (flowplayer) {
						PmsConfiguration configuration = PMS.getConfiguration();
						boolean isFFmpegFontConfig = configuration.isFFmpegFontConfig();
						if (isFFmpegFontConfig) { // do not apply fontconfig to flowplayer subs
							configuration.setFFmpegFontConfig(false);
						}

						OutputParams p = new OutputParams(configuration);
						Player.setAudioAndSubs(r.getName(), r.getMedia(), p);
						if (p.sid !=null && p.sid.getType().isText()) {
							try {
								File subFile = FFMpegVideo.getSubtitles(r, r.getMedia(), p, configuration);
								LOGGER.debug("subFile " + subFile);
								subFile = SubtitleUtils.convertSubripToWebVTT(subFile);
								if (subFile != null) {
									sb.append("<track src=\"/subs/").append(subFile.getAbsolutePath()).append("\">");
								}
							} catch (Exception e) {
								LOGGER.debug("error when doing sub file " + e);
							}
						}
						
						configuration.setFFmpegFontConfig(isFFmpegFontConfig); // return back original fontconfig value
					}
					sb.append("</").append(mediaType).append(">").append(CRLF);

					if (flowplayer) {
						sb.append("</div>").append(CRLF);
					}
		sb.append("</div>").append(CRLF);
		sb.append("<a href=\"/raw/").append(rawId).append("\" target=\"_blank\" id=\"DownloadLink\" title=\"Download this video\"></a>").append(CRLF);
		if (flowplayer) {
			sb.append("<script>").append(CRLF);
			sb.append("$(function() {").append(CRLF);
			sb.append("	$(\".player\").flowplayer({").append(CRLF);
			sb.append("		ratio: 9/16,").append(CRLF);
			sb.append("		flashfit: true").append(CRLF);
			sb.append("	});").append(CRLF);
			sb.append("});").append(CRLF);
			if (r.isResume()) {
				sb.append("var api = flowplayer();").append(CRLF);
				sb.append("api.seek(");
				sb.append(r.getResume().getTimeOffset() / 1000);
				sb.append(");").append(CRLF);
			}
			sb.append("</script>").append(CRLF);
		}
		endPage(sb);

		return sb.toString();
	}

	private boolean transMp4(String mime, DLNAMediaInfo media) {
		LOGGER.debug("mp4 profile "+media.getH264Profile());
		return mime.equals("video/mp4") && (configuration.isWebMp4Trans() || media.getAvcAsInt() >= 40);
	}

	@Override
	public void handle(HttpExchange t) throws IOException {
		LOGGER.debug("got a play request " + t.getRequestURI());
		if (RemoteUtil.deny(t)) {
			throw new IOException("Access denied");
		}
		String id;
		id = RemoteUtil.getId("play/", t);
		String response = mkPage(id, t);
		Headers hdr = t.getResponseHeaders();
		hdr.add("Content-Type", "text/html");
		LOGGER.debug("play page " + response);
		t.sendResponseHeaders(200, response.length());
		try (OutputStream os = t.getResponseBody()) {
			os.write(response.getBytes());
		}
	}
}
