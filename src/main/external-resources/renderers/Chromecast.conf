#----------------------------------------------------------------------------
# Profile for Google Chromecast.
# See PS3.conf for a description of all possible configuration options.
# http://www.universalmediaserver.com/forum/viewtopic.php?f=5&t=1454
# Version 1.3
# 
# Authors: DeFlanko, digitalhigh, Tripollite, Einzeln
#
# Known Issues:
# - There will be no subtitles (yet)
# - Audio may not be your preferred language.
# - Seeking support is limited.
# Prerequisites
# - FFMPEG needs to be Primary Transcoder. 
# - Some use Android.conf to work in tandum, I chose to make two new CONFs: Chromecast.conf and Chromecast_Avia.conf
# and its up to the user to disable/modify the Android.conf if they want, other wise 
#
# User-Agent: Mozilla/5.0 (CrKey armv7l 1.3.14651) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/31.0.1650.0 Safari/537.36, Referer: http://receiver.aviatheapp.com/
# User-Agent: Android/4.2.1 UPnP/1.0 Cling/2.0
# User-Agent: Apache-HttpClient/UNAVAILABLE (java 1.4)

RendererName = ChromeCast
RendererIcon = chromecast.png
UserAgentSearch = CrKey
Video = true
Audio = true
Image = true
MaxVideoWidth = 1280
MaxVideoHeight = 720
KeepAspectRatio = true
TranscodeVideo = H264TSAC3
TranscodeAudio = MP3
UseSameExtension = mp4
MaxVideoBitrateMbps = 20
# SeekByTime = True
SeekByTime = exclusive

MediaInfo = true
#====================================VIDEO======================================
Supported = f:mp4	v:mp4|h264	a:aac|mp3	m:video/mp4	n:2
Supported = f:mp3					m:audio/mpeg	n:2             
#====================================AUDIO======================================
Supported = f:jpg					m:image/jpeg
Supported = f:png					m:image/png
Supported = f:gif					m:image/gif
Supported = f:bmp					m:image/bmp
#====================================SUBTITLES==================================
SupportedSubtitlesFormats = WEBVTT

# Supported Media Type: https://developers.google.com/cast/supported_media_types
#	All Google Cast devices at a minimum support the following media types:
#		Video codecs: H.264 High Profile Level 4.1, 4.2 and 5, VP8
#		Audio decoding: HE-AAC, LC-AAC, CELT/Opus, MP3, Vorbis
#		Containers: MP4/CENC, WebM, MPEG-DASH, SmoothStreaming
#		Level 1 DRM support: Widevine, PlayReady
#
# This is the important part (thanks Einzeln):
#   (Make video work) 
#      -map 0:v which maps the first video stream. 
#      -c:v through -level ensure the video stream is encoded properly for chromecast.
#   (Make audio work) 
#      -map 0:a maps the first audio stream. 
#      -c:a = codec in this case we use libmp3lame for MP3 | libvo-aacenc for LC-AAC and -ac is limiting audio channels for casting.
#   (Prevent stutter due to slow encoding) 
#      -preset ultrafast helps your processor encode the video as quickly as possible. 
#      If your processor is fast enough try increasing this. I expect you might get better quality out of your limited bitrate.
#   (Prevent stutter due to slow connection) 
#      -b:v and -buffsize should ensure the encoded video does not exceed a bitrate that can fit through a wifi connection. 
#      These sometimes seem to be ignored in the same way MaxVideoBitrateMBPS does.
#   (output) 
#      -f matroska, I know chromecast doesn't support mkv. But webm is basically a dumbed down mkv so this works. 
#      Using mp4 has given me problems for some reason.

# Einzeln's Custom (Known Working Config)
CustomFFmpegOptions = -async 1 -fflags +genpts -map 0:v -c:v libx264 -profile:v high -level 4.1 -map 0:a -c:a libmp3lame -ac 2 -preset ultrafast -b:v 35000k -bufsize 35000k -f matroska
